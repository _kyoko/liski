Entity = {}

function Entity.new(image, x, y)
  local e = {
    x = x,
    y = y,
    y_velocity = 0,
    speed = 150,
    jump_height = 300,
    image = image,
    image_state = {
      r = 0,
      sx = 0,
      sy = 0,
      ox = 0,
      oy = 0,
    },
    draw_func = nil,
  }

  local _, h = e.image:getDimensions()
  e.jump_height = h * 20

  setmetatable(e, {__index = Entity})
  return e
end

function Entity:draw(r, sx, sy, ox, oy)
  -- TODO: optional before/after functions? (for colors etc)
  self.draw_func = function()
    love.graphics.draw(self.image, self.x, -self.y, r, sx, sy, ox, oy)
    self.image_state.r = r
    self.image_state.sx = sx
    self.image_state.sy = sy
    self.image_state.ox = ox
    self.image_state.oy = oy
  end
end

function Entity:move(dt)
  if (dt < 0 and self.image_state.sx > 0) or (dt > 0 and self.image_state.sx < 0) then
    local w, _ = self.image:getDimensions()
    if dt < 0 then
      self.x = self.x + w
    else
      self.x = self.x - w
    end
    self:draw(self.image_state.r, -self.image_state.sx, self.image_state.sy, self.image_state.ox, self.image_state.oy)
  end

  self.x = self.x + (self.speed * dt)
end

function Entity:jump(dt)
  self.y_velocity = self.jump_height
end

function Entity:on_ground()
  return self.y <= 0
end

function Entity:tick(dt)
  if not self:on_ground() or self.y_velocity ~= 0 then
    self.y = self.y + self.y_velocity * dt
    self.y_velocity = self.y_velocity - gravity * dt
  end
  if self:on_ground() and self.y_velocity ~= 0 then
    self.y_velocity = 0
    self.y = 0
  end
end

function Entity:tick_draw()
  if self.draw_func then
    self.draw_func()
  end
end
