require "strict"

require "entity"
require "stage"

local camera = require "camera"

gravity = nil
winW, winH = nil
unit = nil
player = nil
liske = nil
debug_text = nil
fullscreen = false
stage = nil
drawQueue = {}
draw_dt = 0.025
cur_dt = 0
freeCam = false

function love.load()
  gravity = 800

  winW, winH = love.graphics.getWidth(), love.graphics.getHeight()
  unit = (winW + winH / 2) / 20

  stage = Stage.new(50, 18, 32)

  stage:load_tileset(love.graphics.newImage("assets/tileset.png"))
  stage:set_tile(5, 5, 1)
  stage:set_tile(5, 6, 2)
  stage:set_tile(5, 7, 3)
  stage:set_tile(6, 5, 4)
  stage:set_tile(7, 5, 1)

  for i = 0, stage.width - 1 do -- FIXME: why - 1?
    stage:set_tile(i, 10, 4)
    stage:set_tile(i, 11, 1)
    stage:set_tile(i, 12, 2)
    for j = 13, stage.height do
      stage:set_tile(i, j, 3)
    end
  end

  local fox = love.graphics.newImage("assets/fox.png")
  player = Entity.new(fox, winW / 2 - 80, 50)
  liske = Entity.new(fox, winW / 2 + 80, 50)

  local font = love.graphics.newFont(12)
  debug_text = love.graphics.newText(font, "")

  camera.setBoundary(0, 0, stage:get_real_dimensions())

  player:draw(0, 1, 1, 0, 0)
  liske:draw(0, -1, 1, 0, 0)
end

function love.draw()
  camera.draw(function()
    stage:tick_draw()

    love.graphics.translate(0, winH / 2)

    player:tick_draw()
    love.graphics.setColor(0x58, 0xed, 0x10)
    liske:tick_draw()
    love.graphics.setColor(0xff, 0xff, 0xff)
  end)

  if next(drawQueue) ~= nil and cur_dt >= draw_dt then
    table.remove(drawQueue, 1)()
    cur_dt = 0
  end

  love.graphics.draw(debug_text, 0, 0, 0, 1, 1, 0, 0)
end

function love.update(dt)
  local w, _ = player.image:getDimensions()
  if not freeCam then
    if player.image_state.sx < 0 then
      camera.lookAt(player.x - w, player.y)
    else
      camera.lookAt(player.x, player.y)
    end
  end
  cur_dt = cur_dt + dt

  if love.keyboard.isDown('left','a') then
    player:move(-dt)
  elseif love.keyboard.isDown('right','d') then
    player:move(dt)
  end

  if love.keyboard.isDown('up', 'w') and player.y_velocity == 0 then
    player:jump(dt)
  end

  player:tick(dt)
  debug_text:set(
    string.format(
      "x %f\ny %f\nvelocity %f\nsx %f\nsy %f\n\ncam x %f\ncam y %f",
      player.x,
      player.y,
      player.y_velocity,
      player.image_state.sx,
      player.image_state.sy,
      camera.getViewport()
    )
  )
  liske:tick(dt)
end

function love.keyreleased(key)
  if key == 'escape' or key == 'q' then
    love.event.push('quit')
  elseif key == 'f' then
    fullscreen = not fullscreen
    love.window.setMode(winW, winH, {fullscreen = fullscreen})
  elseif key == 'g' then
    freeCam = false
  elseif key == 'v' then
    freeCam = true

    local x, y = camera.getViewport()

    local camMove = function(f, x, m)
      table.insert(drawQueue, function() camera.setViewport(x, y) end)
      if m >= 1 then
        f(f, x + m, m / 2)
      end
    end

    camMove(camMove, x, 32)
  elseif key == 'c' then
    freeCam = true

    local x, y = camera.getViewport()

    local camMove = function(f, x, m)
      table.insert(drawQueue, function() camera.setViewport(x, y) end)
      if m >= 1 then
        f(f, x - m, m / 2)
      end
    end

    camMove(camMove, x, 32)
  end
end
