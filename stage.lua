Stage = {}

function Stage.new(w, h, tile_size)
  local xt = tile_size * w
  local yt = tile_size * h

  local s = {
    tile_size = tile_size,
    width = w,
    height = h,
    tileset_img = nil,
    tileset = {},
    tiles = {},
  }

  for i = 0, s.width do
    s.tiles[i] = {}
  end
  
  setmetatable(s, {__index = Stage})
  return s
end

function Stage:set_tile(x, y, tile)
  if x > self.width or y > self.height then
    error("tried to change a tile outside of the stage")
  end

  self.tiles[x][y] = tile
end

function Stage:get_real_dimensions()
  return self.width * self.tile_size, self.height * self.tile_size
end

-- XXX: move these two somewhere else?
function Stage:load_tileset(image)
    local w, h = image:getDimensions()

    self.tileset_img = image

    for x = 0, w-1, self.tile_size do
      for y = 0, h-1, self.tile_size do
        self.tileset[#self.tileset+1] = love.graphics.newQuad(x, y, self.tile_size, self.tile_size, w, h)
      end
    end
end

function Stage:tick_draw()
  for x, v in pairs(self.tiles) do
    for y, tile in pairs(v) do
      if tile then
        love.graphics.draw(self.tileset_img, self.tileset[tile], x*self.tile_size, y*self.tile_size)
      end
    end
  end
end
